import md5 from 'js-md5';
const appid = "cpp.c362a8654d8c81599fda7dd28e52";
const appsecret = "b842276c65314a80b5df49514e3a71ef";

export function EncryptMd5Str(data,time_stamp) {
  const str = JSON.stringify(data) + appid + appsecret + time_stamp;
  const hexMd5Str = md5(str).toUpperCase();
  return hexMd5Str.substring(8,24)
}

export function  getSignObj(data) {
  const time_stamp = Date.parse(new Date())/1e3;
  const result = {
    data:data,
    sign:EncryptMd5Str(data,time_stamp),
    appid:appid,
    time_stamp:time_stamp
  }
  console.log(JSON.stringify(result))
  return result;
}



