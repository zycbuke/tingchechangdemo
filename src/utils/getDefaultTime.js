import data from "../views/system/dict/data";

Date.prototype.Format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1,                 //月份
    "d+": this.getDate(),                    //日
    "h+": this.getHours(),                   //小时
    "m+": this.getMinutes(),                 //分
    "s+": this.getSeconds(),                 //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds()             //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

export function formatTimeToStr(times, pattern) {
  var d = new Date(times).Format("yyyy-MM-dd hh:mm:ss");
  if (pattern) {
    d = new Date(times).Format(pattern);
  }
  return d.toLocaleString();
}

export function getStartTime() {
  var d = new Date()
  return d.Format("yyyy-MM-dd hh:mm:ss");
}

export function getEndTime() {
  var d = new Date();
  d.setDate(d.getDate() + 1);
  return d.Format("yyyy-MM-dd hh:mm:ss");
}

// 获取本月最后一天
export function getmonthEndDate() {
  const d = new Date(); //当前日期
  //本月的结束时间
  const monthEndDate = new Date(d.getFullYear(), d.getMonth() + 1, 0, 23, 59, 59, 999);
  return monthEndDate.Format("yyyy-MM-dd hh:mm:ss");
}

// 通过传入的Time,去掉毫秒
export function getQFTime(qfTime) {
  if (qfTime.indexOf(".") > -1) {
    return qfTime.slice(0, qfTime.indexOf("."));
  } else {
    return qfTime;
  }
}



// 时间为当天的情况
export function getbeginTime() {
  var d = new Date()
  d  =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 1);
  return d.Format("yyyy-MM-dd hh:mm:ss");
}
export function getstopTime() {
  var d = new Date();
  d  =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59, 59, 999);
  return d.Format("yyyy-MM-dd hh:mm:ss");
}






