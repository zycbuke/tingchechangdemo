import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";
import {getSignObj} from '@/utils/EncryptMd5';
import {Encrypt, Decrypt} from '@/utils/asencrypt';
import {getToken} from '@/utils/auth'

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/system/user/' + praseStrEmpty(userId),
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'delete'
  })
}

// 导出用户
export function exportUser(query) {
  return request({
    url: '/system/user/export',
    method: 'get',
    params: query
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status
  }
  return request({
    url: '/system/user/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息
// export function getUserProfile() {
//   return request({
//     url: '/system/user/profile',
//     method: 'get'
//   })
// }
export function getUserProfile(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/getuser',
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改用户个人信息
// export function updateUserProfile(data) {
//   return request({
//     url: '/system/user/profile',
//     method: 'put',
//     data: data
//   })
// }
export function updateUserProfile(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'osauthen/man/setuser',
    method: 'post',
    data: getSignObj(data)
  })
}

// 用户密码重置
// export function updateUserPwd(oldPassword, newPassword) {
//   const data = {
//     oldPassword,
//     newPassword
//   }
//   return request({
//     url: '/system/user/profile/updatePwd',
//     method: 'put',
//     params: data
//   })
// }
export function updateUserPwd(oldPassword, newPassword) {
  const data = {};
  data.token = getToken();
  data.old = Encrypt(oldPassword);
  data.new = Encrypt(newPassword);
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'osauthen/setpass',
    method: 'post',
    data: getSignObj(data)
  })
}


// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/system/user/importTemplate',
    method: 'get'
  })
}
