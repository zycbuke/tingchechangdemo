import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5'
import {getToken} from '@/utils/auth'

// 查询菜单列表
export function listMenu(sys) {
  const data = {
    sys: sys,
    token: getToken()
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/menu/list',
    method: 'post',
    data: getSignObj(data)
  })
}

// 查询平台列表
export function syslist() {
  const data = {
    report: "sys.list",
    param: {
      format: "json",
      token: getToken()
    }
  }
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 查询菜单详细
export function getMenu(menuId) {
  return request({
    url: '/system/menu/' + menuId,
    method: 'get'
  })
}

// 查询菜单下拉树结构
export function treeselect() {
  return request({
    url: '/system/menu/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId) {
  return request({
    url: '/system/menu/roleMenuTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增菜单
export function addMenu(data, sys) {
  data.token = getToken();
  data.sys = sys;
  const box = {
    icon: "",
    isFrame: "",
    menuName: "",
    menuType: "",
    orderNum: "",
    path: "",
    status: "",
    sys: "",
    token: "",
    visible: "",
    component: "",
    parentId: 0
  }
  for (let key in box) {
    if (data[key]) {
      box[key] = data[key]
    }
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/menu/add',
    method: 'post',
    data: getSignObj(box)
  })
}

// 修改菜单
export function updateMenu(data) {
  data.token = getToken();
  const box = {
    token: "",
    icon: "",
    isFrame: "",
    menuName: "",
    menuType: "",
    orderNum: "",
    path: "",
    status: "",
    visible: "",
    component: "",
    parentId: 0,
    menuId:0
  }
  for (let key in box) {
    if (data[key]) {
      box[key] = data[key]
    }
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/menu/set',
    method: 'post',
    data: getSignObj(box)
  })
}

// 删除菜单
export function delMenu(menuId) {
  const data = {
    menuId: menuId,
    token: getToken()
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/menu/del',
    data: getSignObj(data),
    method: 'post',
  })
}

// 获取路由
export function getRouters(sys) {
  const data = {
    sys: sys,
    token: getToken()
  }
  return request({
    url: 'osauthen/menu/route',
    data: getSignObj(data),
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY
  })
}


