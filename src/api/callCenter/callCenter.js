import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

/** 获取呼叫中心列表*/
export function getCenterList() {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/center/list',
    method: 'post',
    data: getSignObj({token: getToken()})
  })
}
/** 添加呼叫中心*/
export function addCallCenter(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/center/add',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改呼叫中心*/
export function editCallCenter(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/center/set',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 根据系统名查询用户列表*/
export function findSysUser(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/userlist',
    method: 'post',
    data: getSignObj(obj)
  })
}
