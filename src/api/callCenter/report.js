import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

/** 获取坐席活动报表列表*/
export function queryReport(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 查询分页信息
export function queryPage(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API +'report/matedata',
    method: 'post',
    data:getSignObj(data)
  })
}
