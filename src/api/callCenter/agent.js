import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

/** 获取呼叫中心列表*/
export function getCenterList() {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/center/list',
    method: 'post',
    data: getSignObj({token: getToken()})
  })
}
/** 获取坐席客服列表*/
export function getAgentList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent/list',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 添加坐席客服*/
export function addAgent(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent/add',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改客服信息*/
export function editAgent(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent/set',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 停用客服坐席*/
export function stopAgent(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent/disable',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 恢复坐席客服*/
export function startAgent(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent/enable',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 读取坐席列表*/
export function getSeatList() {
  const obj ={}
  obj.token = getToken()
  obj.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent-station/list',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 添加一个坐席*/
export function addSeatList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent-station/add',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改坐席*/
export function editSeatList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'ccman/agent-station/set',
    method: 'post',
    data: getSignObj(obj)
  })
}


