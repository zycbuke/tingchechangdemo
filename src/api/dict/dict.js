import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5'

export function getDictList(dict) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'dict/'+dict,
    method: 'get'
  })
}
