import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 查询在场记录
export function listData(param) {
  const data = {
    report: 'parking',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 查询分页信息
export function queryPage(param) {
  const data = {
    report: 'parking',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改停车类型
export function parktype(parkingno, pt) {
  const data = {
    token: getToken(),
    parkingno: parkingno,
    pt: pt
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/parking/parktype',
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改车牌号
export function tag(parkingno, tag) {
  const data = {
    token: getToken(),
    parkingno: parkingno,
    tag: tag //正确车牌号
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/parking/tag',
    method: 'post',
    data: getSignObj(data)
  })
}

// 强制出场
export function out(parkingno, memo) {
  const data = {
    token: getToken(),
    parkingno: parkingno,
    // memo: memo //正确车牌号
  }
  if (memo) {
    data.memo = memo;
  } else {
    data.memo = "";
  }

  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/parking/out',
    method: 'post',
    data: getSignObj(data)
  })
}




