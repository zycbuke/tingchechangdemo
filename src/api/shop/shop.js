import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 获取商家购券订单余量 */
export function getOrder(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/order/list',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 获取停车场商家列表*/
export function getShopList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/list',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 获取商家购券记录查询*/
export function getCoupon(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 商家待支付订单get*/
export function getOrderGet(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/order/get',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 商家自动领券*/
export function grantVehicle(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/grant-vehicle',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 删除商家未结算订单*/
export function delOrder(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/order/del',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 获取商家发券记录查询*/
export function getCouponDetail(obj) {
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 获取团体券列表*/
export function getGroupCouponList(obj) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/group-coupon/list',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 获取团体券*/
export function getGroupCouponLog(obj) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/group-coupon/get',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 创建团体券*/
export function createGroupCoupon(obj) {
  // obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/shop/group-coupon/create',
    method: 'post',
    data:getSignObj(obj)
  })
}
