import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'


// 查询分页信息
export function queryPage(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API +'report/matedata',
    method: 'post',
    data:getSignObj(data)
  })
}

// 获取数据 API
export function queryReport(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}
// 获取数据
export function query(data) {
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取数据 APIGATEWAY
// ex： str= 'parkman/shop/order/list'
export function getList(obj,str) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + str,
    method: 'post',
    data:getSignObj(obj)
  })
}
