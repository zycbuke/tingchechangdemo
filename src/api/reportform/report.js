import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 获取路由
export function queryReport(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}
// 出场查询
export function leaveFind(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
// 查询分页信息
export function queryPage(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API +'report/matedata',
    method: 'post',
    data:getSignObj(data)
  })
}
// 线上支付统计
export function statistic(data) {
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(data)
  })
}
export function couponUnsetPage(obj) {
  return request({
    url: process.env.VUE_APP_BASE_API +'report/matedata',
    method: 'post',
    data:getSignObj(obj)
  })
}
// 商家未结算抵扣券
export function couponUnset(obj) {
  /*obj.param.token = getToken()
  obj.param.format = 'json'*/
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
// 车场营收统计
export function parktoll(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 商家发券日报统计*/
export function couponStatistics(obj) {
  obj.param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}

// 获取操作方式
export function getParkMan() {
  const data = {token: getToken()}
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'dict/parkman',
    method: 'get',
    data: getSignObj(data)
  })
}
