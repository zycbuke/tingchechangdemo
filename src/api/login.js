import request from '@/utils/request'
import {Encrypt, Decrypt} from '@/utils/asencrypt';
import {getSignObj} from '@/utils/EncryptMd5';

// 登录方法
export function login(sysuser, password, portal) {
  const data = {
    sysuser,
    password,
    portal
  }
  data.password = Encrypt(data.password);
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/login',
    method: 'post',
    data: getSignObj(data)
  })
}
// 商家登陆
export function shopLogin(mobile, password) {
  const data = {
    mobile,
  }
  data.pass = password;
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/login',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取用户详细信息
export function getInfo(token) {
  const data = {
    token: token
  }
  return request({
    url: 'osauthen/session',
    method: 'post',
    data: getSignObj(data),
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY
  })
}


// 停车场管理者获取单个停车场信息
export function getParkingInfo(username, token) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/get',
    method: 'post',
    data: getSignObj({"token": token, "name": username})
  })
}

// 停车场业主获取多个停车场信息
export function getParkingList(username, token) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/owner/get',
    method: 'post',
    data: getSignObj({"token": token, "name": username})
  })
}
// 获取平台支持的停车费计算器的名称列表
export function getCalculatorList() {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'calculator/list',
    method: 'get',
  })
}

// 退出方法
export function logout(token) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/logout',
    method: 'post',
    data: getSignObj({"token": token})
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    method: 'get'
  })
}


