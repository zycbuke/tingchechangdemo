import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

// 获取对应的相机列表
export function getCameraList(park, port) {
  const data = {
    token: getToken(),
    park: park,
    port: port
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/camera/list',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取对应的相机
export function getCamera(park, port, ct) {
  const data = {
    token: getToken(),
    park: park,
    port: port,
    ct: ct
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/camera/get',
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改对应的相机
export function setCamera(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/camera/set',
    method: 'post',
    data: getSignObj(form)
  })
}


