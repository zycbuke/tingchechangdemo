import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 获取详细设备信息*/
export function getEquipment(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 批量注册设备*/
export function registerLot(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'deviceman/batchregister',
    method: 'post',
    data:getSignObj(data)
  })
}
/** 注册一个设备*/
export function registerEquipment(name) {
  const param = {
    token: getToken(),
    name: name
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'deviceman/register',
    method: 'post',
    data:getSignObj(param)
  })
}
/** 设备连线时间线*/
export function getEquipTimeline(data) {
  data.param.token = getToken()
  data.param.format = 'json'
  return request({
    url: 'report/query',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_API,
    data: getSignObj(data)
  })
}

/** 设备出货*/
export function getEquipShip(data) {
  data.token = getToken()
  return request({
    method: 'post',
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'deviceman/ship',
    data: getSignObj(data)
  })
}
