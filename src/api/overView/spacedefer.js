import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

//建立车位授权/延期订单
export function spaceGrant(form,isDefer) {
  form.items = "";
  form.token = getToken();
  let url = isDefer?'parkman/defer/order/space-defer':'parkman/defer/order/space-grant'
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + url,
    method: 'post',
    data: getSignObj(form)
  })
}

//获取授权车位信息
export function getEmpowerSpace(param) {
  const data = {
    report :'defer.order.one',
    param: param
  }
  data.param.token = getToken()
  data.param.format = "json"
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取车位号信息
export function getpsidList(param) {
  const data = {
    report :'park.space',
    param: param
  }
  data.param.token = getToken()
  data.param.format = "json"
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

