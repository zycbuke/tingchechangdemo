import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

//
export function listData(param) {
  const data = {
    report: 'vehicle',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}//
export function queryPage(param) {
  const data = {
    report: 'vehicle',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}
//根据停车记录查询停车详情
export function queryDetail(no) {
  const data = {
    parkingno: no,
  }
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/parking/detail',
    method: 'post',
    data: getSignObj(data)
  })
}
//
export function delData(id, park) {
  const data = {
    licensetag: id,
    token: getToken(),
    park: park
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/del',
    method: 'post',
    data: getSignObj(data)
  })
}
//
export function GmCar(form) {
  const data = {
    licensetag: form.licensetag,
    token: getToken(),
    park: form.park,
    psid: form.psid
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/gm',
    method: 'post',
    data: getSignObj(data)
  })
}
// 车位模式切换
export function GmSpace(form) {
  const data = {
    token: getToken(),
    park: form.park,
    psid: form.psid,
    licensetag: form.licensetag,
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/gm',
    method: 'post',
    data: getSignObj(data)
  })
}
//
export function addData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/add',
    method: 'post',
    data: getSignObj(form)
  })
}

//
export function setData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/set',
    method: 'post',
    data: getSignObj(form)
  })
}

export function getData(id, park) {
  const data = {
    licensetag: id,
    token: getToken(),
    park: park
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/get',
    method: 'post',
    data: getSignObj(data)
  })
}

// 车辆授权订单
export function empowerOrder(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/vehicle-grant',
    method: 'post',
    data: getSignObj(data)
  })
}


// 车辆延期订单
export function delayOrder(data) {
  delete data.psid
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/vehicle-defer',
    method: 'post',
    data: getSignObj(data)
  })
}

//获取停车类型月租价格设置
export function deferPriceGet(form) {

  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/price/get',
    method: 'post',
    data: getSignObj(form)
  })
}


//建立延期订单\授权订单
export function vehicleGrant(form, isDefer) {
  if(isDefer){
    delete form.psid
  }
  form.items = "";
  form.token = getToken();
  let url = isDefer ? 'parkman/defer/order/vehicle-defer' : 'parkman/defer/order/vehicle-grant'
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + url,
    method: 'post',
    data: getSignObj(form)
  })
}

//支付订单
export function deferOrderPayed(form) {
  form.payid = '';
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/payed',
    method: 'post',
    data: getSignObj(form)
  })
}

//通过车牌获取车辆信息
export function getTag(park, tag) {
  const form = {};
  form.park = park;
  form.licensetag = tag;
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/get',
    method: 'post',
    data: getSignObj(form)
  })
}
//通过车位获取车位信息
export function getPsid(park, psid) {
  const form = {};
  form.park = park;
  form.psid = psid;
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/get',
    method: 'post',
    data: getSignObj(form)
  })
}
//向后台请求支付订单
export function deferOrderPrepay(orderid) {
  const form = {};
  form.orderid = orderid;
  form.sn = 1;
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/prepay',
    method: 'post',
    data: getSignObj(form)
  })
}
//发送车牌号查询是否有停车记录
export function getParkingNo(park,tag) {
  const form = {park:park,licensetag:tag,parkingno:0};
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/parking/get',
    method: 'post',
    data: getSignObj(form)
  })
}

// 修改车牌号
export function editLicenseTag(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/ren',
    method: 'post',
    data: getSignObj(data)
  })
}
// 获取所有车辆
export function getAllCar() {
  const data = {token: getToken()}
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'dict/park_pt',
    method: 'get',
    data: getSignObj(data)
  })
}


