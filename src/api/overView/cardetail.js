import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 获取车主信息
export function getVo(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/get',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取车主车辆信息
export function getVehicleList(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/list',
    method: 'post',
    data: getSignObj(data)
  })
}


// 添加车主车辆信息
export function addVehicleList(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vehicle/add',
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取车主车位信息
export function getSpaceList(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/list',
    method: 'post',
    data: getSignObj(data)
  })
}


// 获取车主信息
export function setVo(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/set',
    method: 'post',
    data: getSignObj(data)
  })
}

// 添加车位信息
export function addspace(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/add',
    method: 'post',
    data: getSignObj(data)
  })
}
