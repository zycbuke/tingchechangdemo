import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 查询在场记录
export function getPriceList(park) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/price/list',
    method: 'post',
    data: getSignObj({token: getToken(), park: park})
  })
}

// 设置
export function setData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/price/set',
    method: 'post',
    data: getSignObj(form)
  })
}

// 设置时获取该项的信息
export function getData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/price/get',
    method: 'post',
    data: getSignObj(form)
  })
}





