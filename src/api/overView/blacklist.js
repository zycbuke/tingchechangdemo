import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 查询黑名单数据列表
export function listData(param) {
  const data = {
    report: 'park.blacklist',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}// 查询黑名单数据分页详情
export function queryPage(param) {
  const data = {
    report: 'park.blacklist',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}

// 删除黑名单数据
export function delData(id, park) {
  const data = {
    licensetag: id,
    token: getToken(),
    park: park
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/blacklist/del',
    method: 'post',
    data: getSignObj(data)
  })
}

// 添加黑名单数据
export function addData(form) {
  form.token = getToken();
  form.level = form.level ? form.level : "";
  form.policy = form.policy ? form.policy : "";
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/blacklist/add',
    method: 'post',
    data: getSignObj(form)
  })
}




