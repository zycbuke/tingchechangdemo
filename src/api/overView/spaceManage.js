import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 新增车位登记*/
export function addSpace(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/add',
    method: 'post',
    data: getSignObj(param)
  })
}
export function editSpace(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/set',
    method: 'post',
    data: getSignObj(param)
  })
}
export function delSpace(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/del',
    method: 'post',
    data: getSignObj(param)
  })
}
export function findSpace(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/space/get',
    method: 'post',
    data: getSignObj(param)
  })
}
