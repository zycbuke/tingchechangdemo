import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

export function queryPage(param) {
  const data = {
    report: 'vehicle.owner',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}


// 获取车主列表
export function get(param) {
  const data = {
    report:'vehicle.owner',
    param
  }
  data.param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}

// 查询车主登记数据列表
export function listData(param) {
  const data = param
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/get',
    method: 'post',
    data: getSignObj(data)
  })
}
// 新增
export function addData(param) {
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/add',
    method: 'post',
    data: getSignObj(param)
  })
}

// 修改
export function editData(param) {
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/set',
    method: 'post',
    data: getSignObj(param)
  })
}

//修改车主id
export function editId(param) {
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/ren',
    method: 'post',
    data: getSignObj(param)
  })
}


// 删除
export function delData(id,park) {
  const param = {
    id,
    park
  };
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vo/del',
    method: 'post',
    data: getSignObj(param)
  })
}


// 查看车主分组
export function listVog(park) {
  const data = {}
  data.token = getToken();
  data.park = park;
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vog/list',
    method: 'post',
    data: getSignObj(data)
  })
}

// 添加车主分组
export function addVog(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vog/add',
    method: 'post',
    data: getSignObj(data)
  })
}


// 删除车主分组
export function delvog(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vog/del',
    method: 'post',
    data: getSignObj(data)
  })
}
// 修改车主分组名
export function changevog(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/vog/ren',
    method: 'post',
    data: getSignObj(data)
  })
}





