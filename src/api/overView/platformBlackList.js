import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 获取平台黑名单列表*/
export function getpfBlackList(obj) {
  obj.param.token = getToken()
  obj.param.format = "json"
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 添加平台黑名单*/
export function addPfBlackList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/blacklist/add',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 删除平台黑名单*/
export function delPfBlackList(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/blacklist/del',
    method: 'post',
    data: getSignObj(obj)
  })
}
