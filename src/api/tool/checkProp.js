// 检查电话号码
let checkPhone = (rule, value,callback) => {
  const phonereg = /^1[3-9][0-9]{9}$/;
  if (phonereg.test(value)) {
    callback();
  }
  if (value === ''){
    callback(new Error('请输入电话号码'))
  }
  callback(new Error('请输入正确的11位电话号码'));
};

// 检查邮箱
let checkMail = (rule, value, callback) => {
  if (!value) {
    return callback(new Error('邮件不能为空'))
  }
  const pattern = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/
  if (value === ''){
    callback(new Error('请输入电话号码'))
  }
  callback(new Error('请输入正确的11位电话号码'));
  setTimeout(() => {
    if (!pattern.test(value)) {
      callback(new Error('请输入正确的邮箱'));
    } else {
      callback();
    }
  }, 1000);
};


// 检查车牌号
let checkTag = (rule, value, callback) => {
  let str1 = /^[京津晋冀蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼渝川贵云藏陕甘青宁新][ABCDEFGHJKLMNPQRSTUVWXY][\dABCDEFGHJKLNMxPQRSTUVWXYZ]{5}$/;
  // 新能源
  let str2 = /^[京津晋冀蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼渝川贵云藏陕甘青宁新][ABCDEFGHJKLMNPQRSTUVWXY][1-9DF][1-9ABCDEFGHJKLMNPQRSTUVWXYZ]\d{3}[1-9DF]$/
  const reg1 = new RegExp(str1);
  const reg2 = new RegExp(str2);
  if (reg1.test(value) || reg2.test(value)) {
    callback();
  } else {
    callback(new Error('请输入正确的车牌号格式'));
  }
};

module.exports = { checkPhone, checkMail, checkTag };
