import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

// 停车场管理者获取单个停车场信息
export function getPark(username) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/get',
    method: 'post',
    data: getSignObj({"token": getToken(), "name": username})
  })
}

// 停车场管理者修改单个停车场信息
export function setPark(username) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/get',
    method: 'post',
    data: getSignObj({"token": getToken(), "name": username})
  })
}

// 停车场管理者获取单个停车场车位信息
export function getCarPositionInfo(username) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/counter/get',
    method: 'post',
    data: getSignObj({"token": getToken(), "park": username})
  })
}

// 停车场管理者获取单个停车场地理信息
export function getGeographyInfo(username) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/info/get',
    method: 'post',
    data: getSignObj({"token": getToken(), "park": username})
  })
}

// 停车场管理者修改单个停车场地理信息
export function setGeographyInfo(param) {
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/info/set',
    method: 'post',
    data: getSignObj(param)
  })
}

// 停车场管理者修改单个停车场车位信息
export function setCarPositionInfo(param) {
  delete param.used;
  delete param.pc_fixed_used;
  delete param.pc_temp_used;
  delete param.pc_vip_used;
  param.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/counter/set',
    method: 'post',
    data: getSignObj(param)
  })
}

// 获取指定停车场的车道列表
export function getPortList(park) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/list',
    method: 'post',
    data: getSignObj({"token": getToken(), "park": park})
  })
}

// 启用状态
export function changeUsedStatus(park, name, isused) {
  const data = {
    name: name,
    park: park,
    token: getToken()
  }
  let url = "";
  if (isused == "1") {
    url = "parkman/port/disable"
  } else {
    url = "parkman/port/enable"
  }
  return request({
    url: url,
    data: getSignObj(data),
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY
  })
}


// 添加车道数据
export function addPort(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/add',
    method: 'post',
    data: getSignObj(form)
  })
}

// 修改车道数据
export function editPort(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/set',
    method: 'post',
    data: getSignObj(form)
  })
}

// 指定车道控制器
export function bindMCU(form, form2) {
  form.token = getToken();
  form.park = form2.park;
  form.port = form2.name;
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/bindmcu',
    method: 'post',
    data: getSignObj(form)
  })
}

// 修改车道名称
export function renPort(park, name, newname) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/ren',
    method: 'post',
    data: getSignObj({"token": getToken(), "park": park, "name": name, "new": newname})
  })
}

// 获取单个车道的信息
export function getPort(park, name) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/port/get',
    method: 'post',
    data: getSignObj({"token": getToken(), "park": park, "name": name})
  })
}
// 获取停车场mcu列表
export function getMcu(param) {
  param.token = getToken()
  param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/mcu/list',
    method: 'post',
    data: getSignObj(param)
  })

}

