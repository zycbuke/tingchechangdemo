import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'
/** 获取商家列表*/
export function getBusiness(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/list',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 新增商家*/
export function addBusiness(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/add',
    method: 'post',
    data: getSignObj(obj)
  })
}/** 根据名字获取商家*/
export function getBusinessOne(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/get',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 编辑商家信息*/
export function editBusiness(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/set',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 修改商家名称*/
export function editBusinessName(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/ren',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 停用商家*/
export function stopBusiness(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/disable',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 恢复商家*/
export function recoverBusiness(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/enable',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 商家生成二维码*/
export function qrcode(param) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/qrcode',
    method: 'post',
    data: getSignObj(param)
  })
}

/** 商家发券*/
export function dispense(param) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/dispense',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 修改商家登录密码*/
export function setShopPass(param) {
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/setpass',
    method: 'post',
    data: getSignObj(param)
  })
}

/** 商家按量结算和预充值授权*/
export function shopGrant(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/grant',
    method: 'post',
    data: getSignObj(param)
  })
}

/** 充值商家扎帐*/
export function account(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/balance-settle',
    method: 'post',
    data: getSignObj(param)
  })
}

/** 商家结算*/
export function settleAccount(param) {
  param.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/postpaid-settle',
    method: 'post',
    data: getSignObj(param)
  })
}
/** 现金结算*/
export function shoppayed(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/order/payed',
    method: 'post',
    data: getSignObj(data)
  })
}
/** 发送登录账号短信通知【shop/sms.notify】*/
export function sendMessage(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/sms.notify',
    method: 'post',
    data: getSignObj(data)
  })
}
