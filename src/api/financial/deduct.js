import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 获取停车场抵扣列表
export function deductList(data) {
  data.token = getToken()
  return request({
    url: 'parkman/deduct/list',
    baseURL : process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取单个停车场抵扣
export function getDeductList(data) {
  data.token = getToken()
  return request({
    url: 'parkman/deduct/get',
    baseURL : process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 添加抵扣登记
export function addDeduct(data) {
  data.token = getToken()
  return request({
    url: 'parkman/deduct/add',
    baseURL : process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改抵扣登记
export function editDeduct(data) {
  data.token = getToken()
  return request({
    url: 'parkman/deduct/set',
    baseURL : process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

export function editDeductren(data) {
  data.token = getToken()
  return request({
    url: 'parkman/deduct/ren',
    baseURL : process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

