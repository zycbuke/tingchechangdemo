import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

export function listData(report,param) {
  const data = {
    report: report,
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}//
export function queryPage(report,param) {
  const data = {
    report: report,
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}
