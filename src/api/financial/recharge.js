import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 商家充值
export function shopcharge(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/order/charge',
    method: 'post',
    data: getSignObj(data)
  })
}

// 包月商家购买抵扣券
export function shopbuyMonthly(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/order/monthly',
    method: 'post',
    data: getSignObj(data)
  })
}
// parkman/shop/
export function shopbuyPackage(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/shop/order/package',
    method: 'post',
    data: getSignObj(data)
  })
}
