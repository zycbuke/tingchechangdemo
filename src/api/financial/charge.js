import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 获取收费日报
export function getDayCharge(data) {
  data.param.token = getToken()
  data.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}
export function getDayChargeAll(data) {
  data.param.token = getToken()
  data.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}
