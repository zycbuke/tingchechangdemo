import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 获取停车场费率列表
export function getFeeruleList(park) {
  const data = {}
  data.token = getToken()
  data.park = park;
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/feerule/list',
    method: 'post',
    data: getSignObj(data)
  })
}

// 读取停费率规则
export function getFeerule(data) {
  data.token = getToken();

  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/feerule/get',
    method: 'post',
    data: getSignObj(data)
  })
}

// 增加停费率规则
export function addFeerule(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/feerule/add',
    method: 'post',
    data: getSignObj(data)
  })
}

// 清除停费率规则
export function delFeerule(data) {
  data.token = getToken();

  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/feerule/del',
    method: 'post',
    data: getSignObj(data)
  })
}

