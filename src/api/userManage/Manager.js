import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'


// 管理员列表信息
export function getManagerList(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/manager/list',
    method: 'post',
    data:getSignObj(data)
  })
}

// 添加管理员
export function addManager(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/manager/add',
    method: 'post',
    data:getSignObj(data)
  })
}
// 删除管理员
export function delManager(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/manager/del',
    method: 'post',
    data:getSignObj(data)
  })
}
// 发送短信通知
export function smsManager(data) {
  data.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY +'parkman/manager/sms.notify',
    method: 'post',
    data:getSignObj(data)
  })
}
