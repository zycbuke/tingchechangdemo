import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 查询用户登录日志*/
export function getSysLog(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 查询用户操作日志*/
export function getLogInfo(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
/** 系统日志*/
export function getSystemLog(obj) {
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API +'report/query',
    method: 'post',
    data:getSignObj(obj)
  })
}
