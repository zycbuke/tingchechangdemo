import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'
import {Encrypt, Decrypt} from '@/utils/asencrypt';

/** 获取用户列表*/
export function getUserList(param) {
  const obj = {
    report: 'sys.user',
    param: param
  }
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(obj)
  })
}/** 获取用户列表*/
export function queryPage(param) {
  const obj = {
    report: 'sys.user',
    param: param
  }
  obj.param.token = getToken()
  obj.param.format = 'json'
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 添加用户*/
export function addUser(obj) {
  obj.token = getToken()
  obj.password = Encrypt(obj.password)
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/adduser',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 删除用户*/
export function delUser(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/deluser',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 获取用户信息*/
export function getUserInfo(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/getuser',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改用户联系方式*/
export function editUserMobile(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/setuser',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 重置密码*/
export function resetUserPassword(obj) {
  obj.token = getToken()
  obj.password = Encrypt(obj.password)
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/setpass',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 用户授权*/
export function getAuthorize(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/grant',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 取消用户授权*/
export function cancelAuthorize(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/revoke',
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 获取角色*/
export function getRole(obj) {
  obj.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'osauthen/man/rolelist',
    method: 'post',
    data: getSignObj(obj)
  })
}
