import request from '@/utils/request'
import {getSignObj} from '@/utils/EncryptMd5';
import {getToken} from '@/utils/auth'

export function getCashierList(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/cashier/list',
    method: 'post',
    data: getSignObj(data)
  })
}
export function delCashier(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/cashier/del',
    method: 'post',
    data: getSignObj(data)
  })
}

export function addCashier(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/cashier/add',
    method: 'post',
    data: getSignObj(data)
  })
}
export function sendMessageCash(data) {
  data.token = getToken()
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/cashier/sms.notify',
    method: 'post',
    data: getSignObj(data)
  })
}
