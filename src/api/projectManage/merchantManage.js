import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 获取商户列表*/
export function merchantManage(data) {
  data.param.format = 'json'
  data.param.token = getToken()

  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}
/** 添加商户*/
export function addMerchant(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/mch/add',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 修改商户名*/
export function editMerchantName(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mch/ren',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 修改商户信息*/
export function editMerchant(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mch/set',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 新增独立商户信息*/
export function addMch(data) {
  data.token = getToken()
  return request({
    url: 'parkman/park/addmch',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 指定商户收款列表*/
export function getReceiptList(data) {
  data.token = getToken()
  data.format = 'json'
  return request({
    url: 'parkman/mchid/list',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 新增商户收款通道*/
export function addReceiptPass(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mchid/add',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 修改商户收款通道*/
export function editReceiptPass(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mchid/set',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 停用收款通道*/
export function stopReceiptPass(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mchid/disable',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 恢复收款通道*/
export function startReceiptPass(data) {
  data.token = getToken()
  return request({
    url: 'parkman/mchid/enable',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(data)
  })
}
/** 获取支付服务商列表*/
export function getIsvList() {
  const obj = {}
  obj.token = getToken()
  obj.format = 'json'
  return request({
    url: 'parkman/isv/list',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(obj)
  })
}
/** 读取服务商信息*/
export function getIsvDetail(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/get',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(obj)
  })
}
/** 获取收款通道信息*/
export function getPassDetail(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/mchid/get',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(obj)
  })
}
/** 5.5.7	指定集团停车场收款商户*/
export function bindmch(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/park/bindmch',
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    data: getSignObj(obj)
  })
}
