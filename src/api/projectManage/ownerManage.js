import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

//获取停车场业主列表
export function getOwnerList(param) {
  const data = {
    report: 'parkowner',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: 'report/query',
    baseURL: process.env.VUE_APP_BASE_API,
    method: 'post',
    data: getSignObj(data)
  })
}
// 页码获取
export function queryPage(param) {
  const data = {
    report: 'parkowner',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: 'report/matedata',
    baseURL: process.env.VUE_APP_BASE_API,
    method: 'post',
    data: getSignObj(data)
  })
}
// 业主停用
export function disableOwner(name) {
  const data = {
    token: getToken(),
    name: name
  }
  return request({
    url: 'parkman/owner/disable',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 业主启用
export function eableOwner(name) {
  const data = {
    token: getToken(),
    name: name
  }
  return request({
    url: 'parkman/owner/enable',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 新增业主
export function addOwner(data) {
  data.token = getToken();
  return request({
    url: 'parkman/owner/add',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 修改业主信息
export function editOwner(data) {
  data.token = getToken();
  return request({
    url: 'parkman/owner/set',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}
// 修改业主名称
export function editOwnerRen(data) {
  data.token = getToken();
  return request({
    url: 'parkman/owner/ren',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取业主信息
export function getOwner(data) {
  data.token = getToken();
  return request({
    url: 'parkman/owner/get',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}
// 获取指定业主商户列表
export function mchlist(data) {
  data.token = getToken();
  data.format = 'json';
  return request({
    url: 'parkman/mch/list',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 获取指定业主停车场列表
export function parklist(data) {
  data.token = getToken();
  data.format = 'json';
  return request({
    url: 'parkman/park/list',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 新增商户信息
export function addpark(data) {
  data.token = getToken();
  return request({
    url: 'parkman/park/add',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}

// 新增商户信息
export function addmchOwner(data) {
  data.token = getToken();
  return request({
    url: 'parkman/owner/addmch',
    baseURL:process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(data)
  })
}
