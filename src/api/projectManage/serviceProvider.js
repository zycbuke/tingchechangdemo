import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

/** 获取支付服务商*/
export function getISV() {
  const obj = {
    token: getToken(),
    format: 'json'
  }
  return request({
    url: 'parkman/isv/list',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 停用支付服务商*/
export function stopIsv(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/disable',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 恢复服务商登记*/
export function startIsv(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/enable',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改服务商名*/
export function editIsvName(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/ren',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 修改服务商信息*/
export function editIsvInfo(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/set',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 添加支付服务商*/
export function addIsv(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/add',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
/** 读取支付服务商信息*/
export function getIsvDetail(obj) {
  obj.token = getToken()
  return request({
    url: 'parkman/isv/get',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY,
    method: 'post',
    data: getSignObj(obj)
  })
}
