import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

// 查询黑名单数据列表
export function listData(param) {
  const data = {
    report: 'park',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}// 查询黑名单数据分页详情
export function queryPage(param) {
  const data = {
    report: 'park',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}

// 添加数据
export function addData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/add',
    method: 'post',
    data: getSignObj(form)
  })
}
// 添加数据
export function editData(form) {
  form.token = getToken();
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/set',
    method: 'post',
    data: getSignObj(form)
  })
}

// 启用状态
export function changeUsedStatus(park, isused) {
  const data = {
    name: park,
    token: getToken()
  }
  let url = "";
  if (isused == "1") {
    url = "parkman/park/disable"
  } else {
    url = "parkman/park/enable"
  }
  return request({
    url: url,
    data: getSignObj(data),
    method: 'post',
    baseURL: process.env.VUE_APP_BASE_APIGATEWAY
  })
}

// 修改停车场名称
export function ren(name, newname) {
  const data = {
    token: getToken(),
    name: name,
    new: newname //正确车牌号
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/park/ren',
    method: 'post',
    data: getSignObj(data)
  })
}





