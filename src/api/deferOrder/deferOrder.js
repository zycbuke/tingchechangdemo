import request from '@/utils/request'
import {getToken} from '@/utils/auth'
import {getSignObj} from '@/utils/EncryptMd5'

export function queryDeferOrder(param) {
  const data = {
    report: 'defer.order',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/query',
    method: 'post',
    data: getSignObj(data)
  })
}
export function metaDataDeferOrder(param) {
  const data = {
    report: 'defer.order',
    param: param
  }
  data.param.token = getToken();
  data.param.format = 'json';
  return request({
    url: process.env.VUE_APP_BASE_API + 'report/matedata',
    method: 'post',
    data: getSignObj(data)
  })
}

export function getDeferOrder(orderid) {
  const data = {
    orderid: orderid,
    token: getToken()
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/get',
    method: 'post',
    data: getSignObj(data)
  })
}
export function deleteOrder(orderid) {
  const data = {
    orderid: orderid,
    token: getToken()
  }
  return request({
    url: process.env.VUE_APP_BASE_APIGATEWAY + 'parkman/defer/order/del',
    method: 'post',
    data: getSignObj(data)
  })
}
