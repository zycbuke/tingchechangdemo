import {login, logout, getInfo, getParkingInfo, getCalculatorList, shopLogin} from '@/api/login'
import {getToken, setToken, removeToken} from '@/utils/auth'
import de from "element-ui/src/locale/lang/de";
import {getBusinessOne} from '@/api/financial/business'
import { getAllCar } from '@/api/overView/carManage'
import { getDict } from "@/api/tool/gen";


const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    permissions: [],
    ptArr: ['临停车辆', '特种车辆', '免费停车', '固定停车', '月租停车', '储值停车', '时段月租', '共用车位', '共享汽车', '车位分享'],
    payPath: [],
    inports: [],
    outports: [],
    park: "",
    parks: [],
    portal: "",
    dmArr: ['小排量', '中排量', '大排量', '新能源', '混合动力'],
    calculatorList: [],
    vst: ['小型车', '中型车', '大型车'],
    vtArr: ['轿车', 'SUV', 'MPV'],
    /** 车位类型*/
    spaceType: ['临停车位', '固定车位','VIP车位'],
    pss: ['标准车位', '小车位', '大车位', '加大车位'],
    picUrl: 'http://t2.qianfan.cn:8080/apigateway/parkman/pic/',
    shop: {},
    /** 保存商户登录成功后商户相关信息*/
    shopInfo: {},
    /** 存储商户登录信息*/
    merchantLoginInfo: {},
    searchPtArr:[]
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      localStorage.setItem('name', name);
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      localStorage.setItem('avatar', avatar);
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    },
    SET_OUTPORTS: (state, outports) => {
      state.outports = outports.split(",")
    },
    SET_INPORTS: (state, inports) => {
      state.inports = inports.split(",")
    },
    SET_PARK: (state, parks) => {
      state.parks = parks.split(",");
      if (state.park == '') {
        state.park = state.parks[0];
      }
      if (state.park == '') {
        state.park = "重庆测试停车场";
        state.parks = ["重庆测试停车场"];
      }
    },
    SET_PORTAL: (state, portal) => {
      state.portal = portal;
    },
    SET_CALCULATORLIST: (state, calculatorList) => {
      state.calculatorList = calculatorList;
    },
    SET_SHOP: (state, shop) => {
      state.shop = shop;
    },
    SET_SEARCHPTARR: (state, searchPtArr) => {
      state.searchPtArr = searchPtArr
    },
    SET_PAYPATH: (state) => {
      let array = [];
      getDict('paypath').then(res => {
        res.data.list.forEach(item => {
          array.push( item.name)
        })
      });
      state.payPath = array;
    },
  },

  actions: {
    // 登录
    Login({commit}, userInfo) {
      const sysuser = userInfo.sysuser.trim();
      const password = userInfo.password;
      const portal = userInfo.portal;
      return new Promise((resolve, reject) => {
        localStorage.setItem('portal', portal);
        if (portal === '商家') {
          shopLogin(sysuser, password).then(res => {
            /** 返回shop、park*/
            if (res.data) {
              if (res.data.shop === '' || res.data.park === '') {
                res.message = '商家密码错误';
                res.success = 0;
                resolve(res)
              } else {
                setToken('shop');
                commit('SET_TOKEN', 'shop');
                localStorage.setItem('shop', '')
                localStorage.setItem('shop', res.data.park + ',' + res.data.shop);
              }
            } else {
              setToken("");
              commit('SET_TOKEN', "");
            }
            resolve(res)
          })
        } else {
          login(sysuser, password, portal).then(res => {
            if (res.data) {
              setToken(res.data.token);
              commit('SET_TOKEN', res.data.token);
            } else {
              setToken("");
              commit('SET_TOKEN', "");
            }
            resolve(res)
          })
        }
      })
    },
    GetShopInfo({commit, state}) {
      let shop = localStorage.getItem('shop').split(",")[1];
      let park = localStorage.getItem('shop').split(",")[0];
      let portal = localStorage.getItem('portal');
      commit('SET_NAME', shop);
      commit('SET_PARK', park);
      commit('SET_PORTAL', portal);
      return new Promise((resolve, reject) => {
        getBusinessOne({park: park, name: shop}).then(res => {
          const avatar = require("@/assets/image/profile.jpg");
          commit('SET_AVATAR', avatar);
          commit('SET_ROLES', ['ROLE_DEFAULT'])
          commit('SET_PERMISSIONS', ["*:*:*"])
          if (res.success == 1) {
            commit('SET_SHOP', res.data);
          }
          resolve(res)
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })


    },
    //获取停车场信息
    GetParkingInfo({commit, state}) {
      return new Promise((resolve, reject) => {
        getParkingInfo(state.park, state.token).then(res => {
          if(res.data){
            commit('SET_OUTPORTS', res.data.outports)
            commit('SET_INPORTS', res.data.inports)
            getAllCar().then(data => {
              commit('SET_SEARCHPTARR', data.data.list);
            })
          }
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },
    //获取计算器列表
    GetCalculatorList({commit, state}) {
      return new Promise((resolve, reject) => {
        getCalculatorList().then(res => {
          commit('SET_CALCULATORLIST', res.data.list);
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },


    // 获取用户信息
    GetInfo({commit, state}) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(res => {
          const avatar = require("@/assets/image/profile.jpg");
          if (res.data && res.data.session) {
            const user = res.data;
            commit('SET_NAME', user.sysuser);
            commit('SET_PORTAL', user.portal);
            commit('SET_AVATAR', avatar);
            commit('SET_PARK', user.parks);
            commit('SET_ROLES', ['ROLE_DEFAULT'])
            commit('SET_PERMISSIONS', ["*:*:*"])
            commit('SET_PAYPATH');
            resolve(res)
          } else {
            const park = localStorage.getItem('shop').split(',')[0]
            const res = {
              data: {
                portal: '商家管理系统',
                parks: park
              }
            }
            commit('SET_AVATAR', avatar);
            commit('SET_NAME', localStorage.getItem('name'))
            commit('SET_PARK', localStorage.getItem('shop').split(',')[1]);
            commit('SET_ROLES', ['ROLE_DEFAULT'])
            commit('SET_PERMISSIONS', ["*:*:*"])
            resolve(res)
          }
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },


    // 退出系统
    LogOut({commit, state}) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          commit('SET_PERMISSIONS', [])
          commit('SET_NAME', '');
          commit('SET_PORTAL', '');
          commit('SET_PARK', '');
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({commit}) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
