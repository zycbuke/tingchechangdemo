import {constantRoutes} from '@/router'
import Layout from '@/layout/index'
import {getRouters} from "@/api/system/menu";
import {getCarPositionInfo} from "@/api/park/info";
import user from "./user";

const permission = {
  state: {
    routes: [],
    addRoutes: [],
    data: [
      {
        children: [{
          name: 'shopList',
          path: 'shopList',
          component: 'shop/shopList',
          meta: {
            icon: 'peoples',
            title: '发券'
          }
        },{
          name: 'autoCoupon',
          path: 'autoCoupon',
          component: 'shop/autoCoupon',
          meta: {
            icon: 'peoples',
            title: '自动领券'
          }
        },{
          name: 'activeCoupon',
          path: 'activeCoupon',
          component: 'shop/activeCoupon',
          meta: {
            icon: 'peoples',
            title: '动态领券码'
          }
        },{
          name: 'staticCoupon',
          path: 'staticCoupon',
          component: 'shop/staticCoupon',
          meta: {
            icon: 'peoples',
            title: '静态领券码'
          }
        },{
          name: 'groupCoupon',
          path: 'groupCoupon',
          component: 'shop/groupCoupon',
          meta: {
            icon: 'peoples',
            title: '可用团体券'
          }
        }
        ],
        component: 'Layout',
        meta: {
          title: '折扣券',
          icon: 'car'
        },
        path: '/shop'
      },
      {
        children: [{
          name: 'order',
          path: 'order',
          component: 'shop/order',
          meta: {
            icon: 'peoples',
            title: '订单列表'
          }
        }],
        component: 'Layout',
        path: '/'
      },
      {
        children: [{
          name: 'couponFind',
          path: 'couponFind',
          component: 'shop/couponFind',
          meta: {
            icon: 'peoples',
            title: '购券记录'
          }
        }],
        component: 'Layout',
        path: '/'
      },
      {
        children: [{
          name: 'shopVehicle',
          path: 'shopVehicle',
          component: 'financial/shopVehicle/index',
          meta: {
            icon: 'build',
            title: '商家授权车辆报表'
          },
        }],
        component: 'Layout',
        path: '/'
      },
      {
        children: [{
          name: 'shopVehicleGrantLog',
          path: 'shopVehicleGrantLog',
          component: 'financial/shopVehicleGrantLog/index',
          meta: {
            icon: 'build',
            title: '商家授权日志'
          },
        }],
        component: 'Layout',
        path: '/'
      },
      {
        children: [{
          name: 'couponUnset',
          path: 'couponUnset',
          component: 'reportform/couponUnset',
          meta: {
            icon: 'peoples',
            title: '商家未结算抵扣券'
          }
        }],
        component: 'Layout',
        path: '/'
      },
      {
        children: [{
          name: 'couponDaily',
          path: 'couponDaily',
          component: 'reportform/couponDaily',
          meta: {
            icon: 'peoples',
            title: '商家发券日报'
          }
        },{
          name: 'dailyDetail',
          path: 'dailyDetail',
          component: 'reportform/couponDaily/detail',
          meta: {
            icon: 'peoples',
            title: '商家发券日报详情'
          },
          hidden: true,
        }],
        component: 'Layout',
        path: '/'
      }
    ]
  },

  mutations: {
    SET_ROUTES: (state, routes) => {
      state.addRoutes = routes
      state.routes = constantRoutes.concat(routes)
    },
  },
  actions: {
    // 生成路由
    GenerateRoutes({commit}, sys) {
      //获取菜单
      return new Promise(resolve => {
        if (sys === '商家管理系统') {
          const data = []
          const accessedRoutes = filterAsyncRouter(permission.state.data);
          accessedRoutes.push({path: '*', redirect: '/404', hidden: true})
          commit('SET_ROUTES', accessedRoutes)
          resolve(accessedRoutes)
        } else {
          // 向后端请求路由数据
          getRouters(sys).then(res => {
            const data = res.data.list;
            getCarPositionInfo(user.state.park).then(res => {
              if (sys === '停车场管理系统' && res.data.pcm_mode === 0) {
                data.forEach(item => {
                  if (item.children) {
                    item.children.forEach(ite => {
                      if (ite.meta.title == '车主管理') {
                        ite.hidden = true
                      }
                    })
                  }
                })
              }
            })
            setTimeout(() => {
              const accessedRoutes = filterAsyncRouter(data);
              accessedRoutes.push({path: '*', redirect: '/404', hidden: true})
              commit('SET_ROUTES', accessedRoutes)
              resolve(accessedRoutes)
            }, 200)
          })
        }
      })
    }
  }
}

// 遍历后台传来的路由字符串，转换为组件对象
function filterAsyncRouter(asyncRouterMap) {
  return asyncRouterMap.filter(route => {
    if (route.component) {
      // Layout组件特殊处理
      if (route.component === 'Layout') {
        route.component = Layout
      }
    else {
        route.component = loadView(route.component)
      }
    }
    if (route.children != null && route.children && route.children.length) {
      route.children = filterAsyncRouter(route.children)
    }
    return true
  })
}

function getRoutersData(list) {
  const box = []
  if (list.length > 0) {
    list.forEach(l => {
      const n = {};
      if (l.menuType === 'M') {
        n.component = "Layout";
        n.alwaysShow = true;
        n.path = '/' + l.path;
      } else {
        n.component = l.component;
        n.path = '/' + l.path;
      }
      if (l.menuType === "C" && l.parentId === 0) {
        n.component = "Layout";
        n.children = [{
          component: l.component,
          meta: {
            icon: l.icon,
            title: l.menuName,
          },
          name: l.path,
          path: l.path
        }]
      } else {
        n.name = l.path;
        n.meta = {};
        n.meta.icon = l.icon;
        n.meta.title = l.menuName;
        n.hidden == (l.visible == '1') ? true : false;
        n.orderNum = l.orderNum;
        if (l.isFrame === '0') {
          n.redirect = 'noRedirect';
        }
        if (l.children && l.children.length > 0) {
          n.children = getRoutersData(l.children);
        }
      }
      box.push(n);
    })
  }
  return box;
}

export const loadView = (view) => { // 路由懒加载
  return (resolve) => require([`@/views/${view}`], resolve)
}

export default permission
