import router from './router'
import store from './store'
import {Message} from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {getToken} from '@/utils/auth'


NProgress.configure({showSpinner: false})

const whiteList = ['/auth-redirect', '/bind', '/register', '/store', '/internalMenu']

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    /* has token*/
    if (to.path === '/store' || to.path === '/internalMenu') {
      next()
      NProgress.done()
    } else {
      if (store.getters.name === '' || from.path === '/store') {
        store.dispatch('GetInfo').then(res => {
          let sys = res.data.portal;
          if(res.data.role && res.data.role.indexOf("营收主管") > -1) {
            sys = '停车场营收报表'
          }
          store.dispatch('GenerateRoutes', sys).then(accessRoutes => {
            router.addRoutes(accessRoutes) // 动态添加可访问路由表
            next({...to, replace: true}) // hack方法 确保addRoutes已完成
          })
          store.dispatch('GetParkingInfo');
          store.dispatch('GetCalculatorList');
        }).catch(err => {
          store.dispatch('FedLogOut').then(() => {
            next({path: '/'})
          })
        })
      }
      next();
      // if (from.path == '/store') {
      //   if (to.path == "/index") {
      //     next();
      //     return;
      //   } else {
      //     debugger;
      //     router.push({name: '/store', redirect: '/index'})
      //     return;
      //   }
      // } else {
      //   debugger;
      //   next();
      // }
      // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
      // if (hasPermission(store.getters.roles, to.meta.roles)) {
      //   next()
      // } else {
      //   next({ path: '/401', replace: true, query: { noGoBack: true }})
      // }
      // 可删 ↑
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next(`/store`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})


